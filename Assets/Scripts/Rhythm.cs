﻿using UnityEngine;
using System.Collections.Generic;
/// <summary>
/// interfaces with Audio Source component adding rhythm functionality
/// </summary>

public class Rhythm : MonoBehaviour {
    
    public float node_speed;

    private AudioSource audioSource;

    private Song song;

    private GameObject catcher_object;

	// Use this for initialization
	void Start () {
        // load song and play
        this.song = Songs.LoadSong("rhythm test2");

        this.audioSource = this.gameObject.AddComponent<AudioSource>();
        this.audioSource.clip = this.song.clip;
        this.audioSource.Play();

        // generate note game objects
        foreach (var node in song.nodes)
        {
            node.game_object = (GameObject)Instantiate(Resources.Load("PreFabs/Note"));
        }

        // create catcher
        this.catcher_object = (GameObject)Instantiate(Resources.Load("PreFabs/Catcher"));
        this.catcher_object.transform.position = this.transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        foreach (var node in this.song.nodes)
        {
            var new_position = -node.GetSeconds(this.song.bpm) * node_speed + this.audioSource.time * node_speed + this.transform.position.x;
            node.game_object.transform.position = new Vector3(new_position, this.transform.position.y);
        }
	}
}
