﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;

public class Song
{
    public Song()
    {
        this.nodes = new List<RhythmNode>();
    }
    public AudioClip clip;
    public float bpm;
    public List<RhythmNode> nodes;
    
}

public class RhythmNode
{
    public float beat;
    public GameObject game_object;

    public RhythmNode(float beat)
    {
        this.beat = beat;
    }

    public float GetSeconds(float bpm)
    {
        return this.beat * 60 / bpm;
    }

}

public class Songs
{
    public static Song LoadSong(string song_name)
    {
        var song = new Song();

        song.clip = (AudioClip)Resources.Load("Music/" + song_name);

        var song_data = ((TextAsset)Resources.Load("Music/" + song_name + " data")).text.Split('\n');
        
        song.bpm = float.Parse(song_data[0]);

        for (int i = 1; i < song_data.Length; i++)
        {
            if (song_data[i].Length > 1)
                song.nodes.Add(new RhythmNode(i + 1));
        }

        return song;
        
    }
}
